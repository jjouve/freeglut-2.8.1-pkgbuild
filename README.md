Provides a way to install freeglut version 2.8.1 on Linux
distributions that uses [pacman](https://wiki.archlinux.org/index.php/Pacman)
for a package manager.

# Usage

See [the ArchWiki page on `makgepkg`](
https://wiki.archlinux.org/index.php/Makepkg#Usage)

You will probably want to ignore `freeglut` updates in your `/etc/pacman.conf`
```
IgnorePkg = freeglut
```
